#! /usr/bin/env python3


# IRFM-Merge-CSV -- Merge IRFM CSV files
# By: Paula Forteza <paula@forteza.fr>
#     Emmanuel Raviart <emmanuel@raviart.com>
#
# Copyright (C) 2017 Paula Forteza & Emmanuel Raviart
# https://framagit.org/parlement-ouvert/irfm-merge-csv
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""Convert CSV file to a plaintext accounting file using Beancount format."""


import argparse
import os
import sys

from beancount.ingest import cache
from beancount.ingest.importers.csv import Col, Importer
from beancount.parser import printer


importer = Importer(
    {
        # The settlement date, the date we should create the posting at.
        Col.DATE: 'date',
        # The date at which the transaction took place.
        Col.TXN_DATE: 'rdate',
        # 'vdate',
        # 'type',
        # 'raw',
        # 'category',
        # The narration fields. Use multiple fields to combine them together.
        Col.NARRATION1: 'label',
        # Col.NARRATION2: 'TODO',
        # Col.NARRATION3: 'TODO',
        # The amount being posted.
        Col.AMOUNT: 'amount',
        # 'card',
        # 'commission',
        # 'original_amount',
        # 'original_currency',
        # 'country',
        # 'investments',
        # '_untitled_',

        # The payee field.
        # Col.PAYEE: 'TODO'

        # # The narration fields. Use multiple fields to combine them together.
        # NARRATION = NARRATION1 = '[NARRATION1]'
        # NARRATION2 = '[NARRATION2]'
        # NARRATION3 = '[NARRATION3]'

        # Debits and credits being posted in separate, dedicated columns.
        # Col.AMOUNT_DEBIT: 'TODO'
        # Col.AMOUNT_CREDIT: 'TODO'

        # The balance amount, after the row has posted.
        # Col.BALANCE: 'TODO'

        # A column which says DEBIT or CREDIT (generally ignored).
        # Col.DRCR: 'TODO'
        # A field to use as a tag name.

        #Col.TAG: 'TODO',
    },
    'Assets:Banque',  # account
    'EUR',  # currency
    # 'id;url;date;rdate;vdate;type;raw;category;label;amount;card;commission;original_amount;original_currency;country;investments',
    [],
    )


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'source_file_path',
        help='path of source CSV file',
        )
    parser.add_argument(
        'target_file_path',
        help='path of target Beancount file',
        )
    args = parser.parse_args()

    # Convert CSV file from french format to US format.
    # Also remove empty values.
    with open(args.source_file_path, encoding='utf-8') as french_csv_file:
        csv_data = french_csv_file.read()
    csv_data = csv_data.replace(',', '.')
    csv_data = csv_data.replace(';', ',')
    csv_data = csv_data.replace('Not available', '')
    csv_data = csv_data.replace('Not loaded', '')
    csv_data = csv_data.replace('NotAvailable', '')
    csv_data = csv_data.replace('NotLoaded', '')
    with open('/tmp/releve.csv', 'w', encoding='utf-8') as target_file:
        target_file.write(csv_data)

    # Convert clean CSV to Beancount journal.
    csv_file = cache.get_file('/tmp/releve.csv')
    entries = importer.extract(csv_file)
    with open(args.target_file_path, 'w', encoding='utf-8') as target_file:
        print('2017-07-01 open Assets:Banque', file=target_file)
        print('', file=target_file)
        for entry in entries:
            entry_string = printer.format_entry(entry)
            print(entry_string, file=target_file)

    return 0


if __name__ == '__main__':
    sys.exit(main())
