# IRFM-Merge-CSV

> IRFM-Merge-CSV merges CSV files collected from an IRFM bank account into a single CSV file without duplicates, generates a plaintext accounting file and then generates a static accounting web site.

IRFM-Merge-CSV is a module that allows members of French Parliament to publish their _frais de mandat_ as open data.

IRFM-Merge-CSV is free and open source software, developped by the french Member of Parliament [Paula Forteza](https://forteza.fr) and her team.
