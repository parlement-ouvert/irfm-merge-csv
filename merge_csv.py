#! /usr/bin/env python3


# IRFM-Merge-CSV -- Merge IRFM CSV files
# By: Paula Forteza <paula@forteza.fr>
#     Emmanuel Raviart <emmanuel@raviart.com>
#
# Copyright (C) 2017 Paula Forteza & Emmanuel Raviart
# https://framagit.org/parlement-ouvert/irfm-merge-csv
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""Merge CSV files collected from an IRFM bank account into a single CSV file without duplicates."""


import argparse
import csv
import os
import sys


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'source_dir',
        help='path of source directory containing CSV files',
        )
    parser.add_argument(
        'target_file_path',
        help='path of target CSV file',
        )
    args = parser.parse_args()

    labels = None
    target_rows = []
    for filename in sorted(os.listdir(args.source_dir)):
        if filename.startswith('.'):
            continue
        if not filename.endswith('.csv'):
            continue
        source_csv_file_path = os.path.join(args.source_dir, filename)
        with open(source_csv_file_path, encoding='utf-8') as source_csv_file:
            csv_reader = csv.reader(source_csv_file, delimiter=';')
            labels = next(csv_reader)
            id_index = labels.index('id')
            date_index = labels.index('date')
            label_index = labels.index('label')
            amount_index = labels.index('amount')
            for row in reversed(list(csv_reader)):
                row = [
                    cell
                        if cell not in ('Not available', 'Not loaded', 'NotAvailable', 'NotLoaded')
                        else ''
                    for cell in row
                    ]
                same_rows = [
                    i
                    for i, target_row in enumerate(target_rows)
                    if target_row[id_index] == row[id_index]
                        and  target_row[date_index] == row[date_index]
                        and  target_row[label_index] == row[label_index]
                        and  target_row[amount_index] == row[amount_index]
                    ]
                if same_rows:
                    target_rows[same_rows[0]] = row
                else:
                    target_rows.append(row)

    with open(args.target_file_path, 'w', encoding='utf-8') as target_csv_file:
        csv_writer = csv.writer(target_csv_file, delimiter=';')
        csv_writer.writerow(labels)
        csv_writer.writerows(reversed(target_rows))

    return 0


if __name__ == '__main__':
    sys.exit(main())
